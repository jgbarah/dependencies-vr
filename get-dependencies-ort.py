#! /usr/bin/env python3

# Analyze the analysis file produced by ORT (OSS Review Toolkit)

import json

with open('analyzer-result.json') as file:
    analysis = json.load(file)
    print(analysis['analyzer']['result']['projects'][0]['id'])
    for package in analysis['analyzer']['result']['packages']:
        print(package['package']['id'])