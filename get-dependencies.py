#! /usr/bin/env python3

# Analyze dependencies produced by
# npm ls -json true > dependencies.json

import json

# Maximum number of dependencies to consider
max_deps = 100000
# Produce pretty (readable) JSON
pretty = True
if pretty:
    indent = 4
    sort_keys = True
else:
    indent = None
    sort_keys = True

def explore(pkg, deps, level):
    deps_list = []
    for name, dep in deps.items():
        if 'version' in dep:
            version = dep['version']
            deps_list.append({'pkg': pkg, 'dep': name,
                              'version': version, 'level': level})
            if 'dependencies' in dep:
                explored_list = explore(f'{name}:{version}',
                                        dep['dependencies'], level+1)
                deps_list.extend(explored_list)
    return deps_list

with open('dependencies.json') as file:
    deps = json.load(file)
    main = deps['name']
    main_version = deps['version']
    pkg = f'{main}:{main_version}'
    deps_list = explore(pkg, deps['dependencies'], 1)
    pkgs = {pkg: 0}
    nodes = []
    links = []
    for dep in deps_list[:max_deps]:
        source = dep['pkg']
        target = dep['dep']
        version = dep['version']
        pkg = f'{target}:{version}'
        level = dep['level']
        if pkg not in pkgs:
            pkgs[pkg] = level
        else:
            if pkgs[pkg] > level:
                pkgs[pkg] = level
        links.append({'id': len(links),
                      'source': source,
                      'target': pkg,
                      'version': version})
    for pkg, level in pkgs.items():
        nodes.append({'name': pkg, 'level': level})
    with open('nodes.json', 'w') as f:
        json.dump(nodes, f, indent=indent, sort_keys=sort_keys)
    with open('links.json', 'w') as f:
        json.dump(links, f, indent=indent, sort_keys=sort_keys)
    print("Output in nodes.json and links.json")