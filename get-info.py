#! /usr/bin/env python3

# Obtain info about an npm package from registry.npmjs.org
# For example, for package react-native 0.64.1 information
# can be obtained from
# https://registry.npmjs.org/react-native/0.64.1

import json
import shelve
import urllib.request

# Produce pretty (readable) JSON
pretty = True
if pretty:
    indent = 4
    sort_keys = True
else:
    indent = None
    sort_keys = True

def retrieve_info(name, version):
    info_json = urllib.request.urlopen(f'https://registry.npmjs.org/{name}/{version}').read()
    info = json.loads(info_json.decode('utf8'))
    return info

def get_info(name, version):
    found = False
    with shelve.open('info') as all_info:
        info = {}
        if name in all_info:
            print("Already in db:", name, all_info[name])
            if version in all_info[name].keys():
                info = all_info[name][version]
                print("Version found:", name, version, info)
                found = True
            else:
                name_info = all_info[name]
        else:
            name_info = {}
        if not found:
            print("Retrieving info for:", name, version)
            info = retrieve_info(name, version)
            name_info[version] = info
            all_info[name] = name_info
            print("Copied to database:", name, version, all_info[name])
        return info

if __name__ == '__main__':
    with open('nodes.json') as file:
        nodes = json.load(file)
        for node in nodes:
            print(node['name'], end=',', flush=True)
            [name, version] = node['name'].split(':')
            info = get_info(name, version)
            node['license'] = info.get('license', "Unknown")
            if 'dist' in info:
                node['files'] = info['dist'].get('fileCount', 0)
                node['size'] = info['dist'].get('unpackedSize', 0)
                node['size3'] = node['size'] ** (1./3)
                node['sizeK'] = node['size'] / 1000
                node['sizeK2'] = node['size'] ** (1./2.5)
            else:
                node['files'] = 0
                node['size'] = 0
                node['size3'] = 0
                node['sizeK'] = 0
                node['sizeK2'] = 0
            print(node)

        with open('nodes-info.json', 'w') as f:
            json.dump(nodes, f, indent=indent, sort_keys=sort_keys)
