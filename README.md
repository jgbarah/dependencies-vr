# Visualizing package dependencies in VR

In the directory of a JavaScript component, with all its dependencies installed:

```
npm ls -json true > dependencies.json
```

Then, from the directory with that `dependencies.json` file, run:

```
get-dependencies.py
get-info.py
```

This will produce `links.json`, `nodes.json` and `nodes-info.json`.
The latter is a nodes file where each node is annotated with some more parameters
(license, size, files, etc.)
